package corso.lezHib.RefactorHibernate.models.crud;

import java.util.List;

public interface Dao<T> {

	public void insert(T t);
	
	public boolean delete(int id);
	
	public boolean delete(T t);
	
	public boolean update(T t);
	
	public T findById(int id);
	
	public List<T> findAll();
}
