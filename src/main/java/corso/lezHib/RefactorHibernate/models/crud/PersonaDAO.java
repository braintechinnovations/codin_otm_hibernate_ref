package corso.lezHib.RefactorHibernate.models.crud;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lezHib.RefactorHibernate.models.Persona;
import corso.lezHib.RefactorHibernate.models.db.GestoreSessioni;

public class PersonaDAO implements Dao<Persona>{

	
	
	@Override
	public void insert(Persona t) {

		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			sessione.save(t);
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Programma terminato");
		}
		
		
	}

	@Override
	public boolean delete(int id) {

		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
			Persona temp = sessione.get(Persona.class, id);
			sessione.delete(temp);
			
			sessione.getTransaction().commit();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Programma terminato");
		}
		
		return false;
		
	}

	@Override
	public boolean delete(Persona t) {

		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
			sessione.delete(t);
			
			sessione.getTransaction().commit();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Programma terminato");
		}
		
		return false;
		
	}

	@Override
	public boolean update(Persona t) {
		
		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
			sessione.update(t);
			
			sessione.getTransaction().commit();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Programma terminato");
		}
		
		return false;
	}

	@Override
	public Persona findById(int id) {

		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		Persona temp = null;
		
		try {
			
			sessione.beginTransaction();
			
			temp = sessione.get(Persona.class, id);
			
			sessione.getTransaction().commit();
						
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Programma terminato");
		}
		
		return temp;
		
	}

	@Override
	public List<Persona> findAll() {

		Session sessione = GestoreSessioni.getInstance().getFactory().getCurrentSession();
		
		List<Persona> elenco = null;
		
		try {
			
			sessione.beginTransaction();
			
			elenco = sessione.createQuery("FROM Persona WHERE per_nome = 'Valeria'").list();
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
		return elenco;
	}

}
