package corso.lezHib.RefactorHibernate;

import corso.lezHib.RefactorHibernate.models.Persona;
import corso.lezHib.RefactorHibernate.models.crud.PersonaDAO;

public class App 
{
    public static void main( String[] args )
    {

    	PersonaDAO perDao = new PersonaDAO();
    	
    	Persona giorgio = new Persona("Giorgio", "Giorgetti");

    	perDao.insert(giorgio);
    	
    	if(giorgio.getId() > 0) {
    		System.out.println("Salvataggio effettuato con successo!");
    	}
    	else {
    		System.out.println("Errore di salvataggio");
    	}
    	
    	

    }
}
